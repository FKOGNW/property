# Property

Property listing application.

## Server Requirements

PHP version 5.6 or newer is recommended.

## Installation

We're using domain `property.dev` while developing this application.

Application entry point: `index.php`, are stored in `public/` directory.

You may setup your environment to provide similar condition.

## License

Please see the [license agreement](LICENSE.md).

## Acknowledgement

This project are build upon CodeIgniter 3.1.4.

This project started to fulfill final assignment from Practice Class of Internet
Programming Course (IK480) at Computer Science Department of Universitas
Pendidikan Indonesia.
