<?php

	/*
	*	Transaski_model.php
	* 	Model untuk tbtransaksi
	*/

	class Transaksi_model extends CI_Model
	{

		function __construct()
		{
			parent::__construct();
		}

		/*fungsi mendapatkan semua data transaksi*/
		function getAllTransaksi(){
			$query = $this->db->get('tbtransaksi');
        	return $query->result();
		}

		function beliProperty($data){
			$this->db->insert('tbtransaksi', $data);
		}

		function bayar($kode_pembayaran, $waktu_bayar){
			$this->db->where('kode_pembayaran', $kode_pembayaran);
	    	$this->db->set('status_bayar', 'sudah');
	    	$this->db->set('waktu_bayar', $waktu_bayar);
	    	$this->db->update('tbtransaksi');
		}

		function getIdProperty($kode_pembayaran){
			$this->db->from('tbtransaksi');
			$this->db->where('kode_pembayaran', $kode_pembayaran);
			return $this->db->get()->row()->id_property;
		}
	}

?>
