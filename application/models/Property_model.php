<?php

	/*
	*	Property_model.php
	* 	Model untuk tbproperty
	*/

	class Property_model extends CI_Model
	{

		function __construct()
		{
			parent::__construct();
		}
		function selectAll()
		{
			$this->db->select('*');
			$this->db->from('tbproperty');
			$this->db->order_by('id', 'ASC');
			return $this->db->get();
		}

		/*fungsi sorting berdasarkan yang diinginkan user*/
		function selectBy($sort){
			$this->db->select('*');
			$this->db->from('tbproperty');
			$this->db->order_by($sort, 'ASC');
			return $this->db->get();
		}

		function selectById($id)
		{
			$this->db->select('*');
			$this->db->from('tbproperty');
			$this->db->where('id',$id);
			return $this->db->get();
		}
		function selectByPriceRange($min, $max)
		{
			$this->db->where('harga >=', $min);
			$this->db->where('harga <=', $max);
			$this->db->from('tbproperty');
			return $this->db->get();
		}


		/*fungsi menambahkan properti*/
		function insertProperty($data){
			$this->db->insert('tbproperty', $data);
		}

		/*fungsi update property*/
		function updateProperty($id, $data)
		{
			$this->db->where('id',$id);
			$this->db->update('tbproperty',$data);
		}

		/*fungsi delete property*/
		function deleteProperty($id){
			$this->db->where('id', $id);
			$this->db->delete('tbproperty');
		}

		/*fungsi searching*/
		public function searchProperty($keyword, $kategori)
	    {
	    	// $this->db->where("kota LIKE '%$keyword%'");
	    	$this->db->like($kategori, $keyword, 'before');
	    	$query= $this->db->get('tbproperty');
	        return $query->result();
	    }

	    /*fungsi bila property sudah terjual*/
	    function terjual($id_property){
	    	$this->db->where('id', $id_property);
	    	$this->db->set('status', 'Sudah terjual');
	    	$this->db->update('tbproperty');
	    }
	}

?>
