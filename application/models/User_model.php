<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllUser()
    {
        $query = $this->db->get('tbuser');
        return $query->result();
    }

    public function loginUser($data) {

		$condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password'] . "' AND " . "kategori = ". "'". "user" . "'";
		$this->db->from('tbuser');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	public function getId($data){
		$condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password'] . "' AND " . "kategori = ". "'". "user" . "'";
		$this->db->from('tbuser');
		$this->db->where($condition);
		return $this->db->get()->row()->id;
	}

	public function getNama($data){
		$condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password'] . "' AND " . "kategori = ". "'". "user" . "'";
		$this->db->from('tbuser');
		$this->db->where($condition);
		return $this->db->get()->row()->nama;
	}

	public function lastLogin($data){
		$this->db->where('username', $data['username']);
	    $this->db->set('last_login', $data['last_login']);
	    $this->db->update('tbuser');
	}

	public function loginAdmin($data){
		$condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password'] . "' AND " . "kategori = ". "'". "admin" . "'";
		$this->db->select('*');
		$this->db->from('tbuser');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	public function insertUser($data){
		$this->db->insert('tbuser', $data);
	}

}
