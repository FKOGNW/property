<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/*
	* Admin.php
	*   Controller untuk Admin
	*/

	class Admin extends CI_Controller
	{

		public function __construct()
		{
			parent::__construct();
		}

		public function index()
		{
			$this->load->view('login_admin');
		}

		public function prosesLogin()
		{
			$data['username'] = $this->input->post('username');
			$data['password'] = md5($this->input->post('password'));
			// $data['last login'] = date('Y-m-d H:i:s', now());
			
			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Jakarta'));
			$data['last_login']= $now->format('Y-m-d H:i:s');
			// $data['last login'] = date('Y-m-d H:i:s', now());

			$this->User_model->lastLogin($data);

			$result = $this->User_model->loginAdmin($data);
			$this->session->set_userdata("logged_in", true);
			if ($result == TRUE) {
				redirect(site_url('Admin/home'));
				// nyimpen id disini
				// $this->session->set_userdata("user_id", $this->User_model->fungsingambilid());
			} else {
				redirect(site_url('Admin'));
			}
		}

		public function logout() {
			$this->session->sess_destroy();
			redirect(site_url('page'));
		}

		public function home(){
			$data['dataTransaksi']= $this->Transaksi_model->getAllTransaksi();
			$this->load->view('home_admin', $data);
		}

		public function pageProperty(){
			$data['dataProperty']= $this->Property_model->selectAll()->result();
			$this->load->view('page_property', $data);
		}

		public function pagePaying(){
			$this->load->view('paying_admin');
		}

		public function prosesPembayaran(){
			$kode_pembayaran= $this->input->post('kode_pembayaran');

			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Jakarta'));
			$waktu_bayar= $now->format('Y-m-d H:i:s');

			$this->Transaksi_model->bayar($kode_pembayaran, $waktu_bayar);
			
			$id_property= $this->Transaksi_model->getIdProperty($kode_pembayaran);
			$this->Property_model->terjual($id_property);

			redirect(site_url('Admin/home'));
		}

		public function tambahProperty(){
			$this->load->view('form_tambah_property');
		}

		public function prosesTambahProperty(){
			$data['pemilik']= $this->input->post('pemilik');
			$data['jalan']= $this->input->post('jalan');
			$data['kelurahan']= $this->input->post('kelurahan');
			$data['kecamatan']= $this->input->post('kecamatan');
			$data['kota']= $this->input->post('kota');
			$data['kode_pos']= $this->input->post('kode_pos');
			$data['telepon']= $this->input->post('telepon');
			$data['nego']= $this->input->post('nego');
			$data['harga']= $this->input->post('harga');
			$data['deskripsi']= $this->input->post('deskripsi');
			$data['tipe']= $this->input->post('tipe');
			$data['koordinat']= $this->input->post('koordinat');
			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Jakarta'));
			$data['created_at']= $now->format('Y-m-d H:i:s');

			$this->Property_model->insertProperty($data);

			redirect(site_url('Admin/pageProperty'));
		}

		public function deleteProperty($id){
			$this->Property_model->deleteProperty($id);

			redirect(site_url('Admin/pageProperty'));
		}

		public function updateProperty($id){
			$data['property']= $this->Property_model->selectById($id)->row();

			$this->load->view('form_edit_property', $data);
		}

		public function prosesUpdateProperty(){
			$data['pemilik']= $this->input->post('pemilik');
			$data['jalan']= $this->input->post('jalan');
			$data['kelurahan']= $this->input->post('kelurahan');
			$data['kecamatan']= $this->input->post('kecamatan');
			$data['kota']= $this->input->post('kota');
			$data['kode_pos']= $this->input->post('kode_pos');
			$data['telepon']= $this->input->post('telepon');
			$data['harga']= $this->input->post('harga');
			$data['nego']= $this->input->post('nego');
			$data['deskripsi']= $this->input->post('deskripsi');
			$data['tipe']= $this->input->post('tipe');
			$id= $this->input->post('id');

			$this->Property_model->updateProperty($id, $data);

			redirect(site_url('Admin/pageProperty'));
		}
	}

?>
