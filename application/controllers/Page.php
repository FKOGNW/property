<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Page extends CI_Controller {

		public function index()
		{
			$data['dataProperty']=$this->Property_model->selectAll()->result();
			$this->load->view('home', $data);
		}

		public function register(){
			$this->load->view('form_daftar_user');
		}

		public function processRegister(){
			$data['nama']= $this->input->post('nama');
			$data['username']= $this->input->post('username');
			$data['password']= md5($this->input->post('password'));
			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Jakarta'));
			$data['created_at']= $now->format('Y-m-d H:i:s');

			$this->User_model->insertUser($data);
			redirect(site_url('Page'));
		}

		public function login(){
			$this->load->view('login_user');
		}

		public function processLogin(){
			$data['username'] = $this->input->post('username');
			$data['password'] = md5($this->input->post('password'));
			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Jakarta'));
			$data['last_login']= $now->format('Y-m-d H:i:s');
			// $data['last login'] = date('Y-m-d H:i:s', now());

			$result = $this->User_model->loginUser($data);
			$this->User_model->lastLogin($data);
			$this->session->set_userdata("logged_in", true);
			$this->session->set_userdata("id_user", $this->User_model->getId($data));
			$this->session->set_userdata("nama_user", $this->User_model->getNama($data));
			if ($result == true) {
				redirect(site_url('Page'));
				$this->session->set_userdata("logged_in", true);
				$this->session->set_userdata("id_user", $this->User_model->getId($data));
				$this->session->set_userdata("nama_user", $this->User_model->getNama($data));
			} else {
				$this->session->sess_destroy();
				redirect(site_url('Page/login'));
			}
		}

		public function logout() {
			$this->session->sess_destroy();
			redirect(site_url('page'));
		}

		public function sortBy(){
			$sort= $this->input->post('sort');

			$data['dataProperty']= $this->Property_model->selectBy($sort)->result();
			$this->load->view('home', $data);
		}

		public function search(){
			$keyword = $this->input->post('keyword');
			$kategori = $this->input->post('kategori');
			$data['dataProperty']=   $this->Property_model->searchProperty($keyword, $kategori);
			$this->load->view('home',$data);
		}

		public function priceRange(){
			$min= $this->input->post('min');
			$max= $this->input->post('max');

			$data['dataProperty']= $this->Property_model->selectByPriceRange($min, $max)->result();
			$this->load->view('home', $data);
		}

		public function detail($id){
			$data['property']= $this->Property_model->selectById($id)->row();

			$this->load->view('detail_page', $data);
		}

		public function buyingProcess(){
			$data['id_user']= $this->input->post('id_user');
			$data['id_property']= $this->input->post('id_property');
			$data['kode_pembayaran']= random_string('alnum', 6);

			$this->Transaksi_model->beliProperty($data);

			$this->load->view('kode_page', $data);
		}
	}
?>
