<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
	<title>Property - Temukan hunian idaman!</title>
	<link rel="stylesheet" href="<?php echo site_url() . 'css/bootstrap.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/font-awesome.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/app.css'; ?>" />
</head>

<body>
<?php include('partials/navbar.php'); ?>

	<div class="jumbotron jumbotron-fluid" style="background-color: white;">
		<div class="container">
			<h1 class="display-4" style="width: 40rem;">Property membantu anda menemukan hunian idaman!</h1>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="container">
					<fieldset class="form-group">
						<legend><h5>Urutkan</h5></legend>
						<form action="<?php echo site_url('Page/sortBy'); ?>" method="post">
							<div class="form-check">
							  <label class="form-check-label">
								<input type="radio" class="form-check-input"  name="sort" value="harga" checked>
							    <span class="ml-2">Harga</span>
							  </label>
							</div>
							<div class="form-check">
							  <label class="form-check-label">
								<input type="radio" class="form-check-input"  name="sort" value="tipe">
							    <span class="ml-2">Tipe</span>
							  </label>
							</div>
							<div class="form-check">
							  <label class="form-check-label">
								<input type="radio" class="form-check-input"  name="sort" value="kota">
								<span class="ml-2">Lokasi</span>
							  </label>
							</div>
							<input class="btn btn-secondary" name="submit" value="sort" type="submit" id="contact-submit" data-submit="...Sending">
						</form>
					</fieldset>
				</div>
				<div class="container">
					<h5>Cari</h5>
					<form id="contact" action="<?php echo site_url('Page/search'); ?>" method="post">
						<div class="form-group row">
							<label for="searchType" class="col-sm-4 col-form-label">Berdasarkan</label>
							<div class="col-sm-8">
								<select name="kategori" id="searchType" class="form-control">
									<option value="kota" >Daerah</option>
									<option value="tipe">Tipe</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="Keyword" class="col-sm-4 col-form-label">Keyword</label>
							<div class="col-sm-8">
								<input id="Keyword" name = "keyword" type="text" placeholder="Search here..." tabindex="1" required autofocus class="form-control">
							</div>
						</div>
						<button class="btn btn-secondary" name="submit" value="tambah" type="submit" id="contact-submit" data-submit="...Sending">Search</button>
					</form>
				</div>
				<div class="container mt-5">
					<h5>Cari Berdasarkan Harga.</h5>
					<form id="contact" action="<?php echo site_url('Page/priceRange'); ?>" method="post">
						<div class="form-group row">
							<label for="inputMin" class="col-sm-4 col-form-label">Termurah</label>
							<div class="col-sm-8">
								<input id="inputMin" placeholder="Max" name="min" type="number" min="0" tabindex="1" required autofocus class="form-control">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputMax" class="col-sm-4 col-form-label">Termahal</label>
							<div class="col-sm-8">
								<input id="inputMax" placeholder="Max" name="max" type="number" min="0" tabindex="1" required autofocus class="form-control">
							</div>
						</div>
						<button class="btn btn-secondary" name="submit" value="tambah" type="submit" id="contact-submit" data-submit="...Sending">Search</button>
					</form>
				</div>
			</div>
			<div class="col-md-8">
				<?php if(empty($dataProperty)){?>
					<p>Maaf data yang anda cari tidak ada</p>
				<?php } else{?>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Nama Pemilik</th>
							<th>Kota</th>
							<th>Tipe</th>
							<th>Harga</th>
							<th>Status</th>
							<th>Opsi</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($dataProperty as $property) { ?>
						<tr>
							<td><?php echo $property->pemilik; ?></td>
							<td><?php echo $property->kota; ?></td>
							<td><?php echo $property->tipe; ?></td>
							<td><?php echo $property->harga; ?></td>
							<td><?php echo $property->status; ?></td>
							<td>
								<a href="<?php echo site_url('Page/detail/' .$property->id); ?>">Detail</a>
							</td>
						</tr>
					<?php } ?>
					</tbody>

				</table>
				<?php  }?>
			</div>
		</div>
	</div>

	<script src="<?php echo site_url() . 'js/libs/jquery.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/tether.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/bootstrap.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/script.js'; ?>"></script>
</body>

</html>

