<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
	<title>Property - Temukan hunian idaman!</title>
	<link rel="stylesheet" href="<?php echo site_url() . 'css/bootstrap.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/font-awesome.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/app.css'; ?>" />
</head>

<body>
	<?php include('partials/navbar_admin.php'); ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="container">
					<form id="contact" action="<?php echo site_url('Admin/prosesTambahProperty'); ?>" method="post">
						<h3>Form Tambah Property</h3>
						<div class="form-group row">
							<div class="col-md-2 col-form-label">Nama pemilik : </div>
							<div class="col-md-10 "><input placeholder="Nama pemilik" name="pemilik" type="text" tabindex="1" required autofocus></div>
						</div>
						<div class="form-group row">
							<div class="col-md-2 col-form-label">Jalan : </div>
							<div class="col-md-10 "><input placeholder="Jalan" name="jalan" type="text" tabindex="1" required autofocus></div>
						</div>
						<div class="form-group row">
							<div class="col-md-2 col-form-label">Kelurahan : </div>
							<div class="col-md-10 "><input placeholder="Kelurahan" name="kelurahan" type="text" tabindex="1" required autofocus></div>
						</div>
						<div class="form-group row">
							<div class="col-md-2 col-form-label">Kecamatan : </div>
							<div class="col-md-10 "><input placeholder="Kecamatan" name="kecamatan" type="text" tabindex="1" required autofocus></div>
						</div>
						<div class="form-group row">
							<div class="col-md-2 col-form-label">Kota : </div>
							<div class="col-md-10 "><input placeholder="Kota" name="kota" type="text" tabindex="1" required autofocus></div>
						</div>
						<div class="form-group row">
							<div class="col-md-2 col-form-label">Kode pos : </div>
							<div class="col-md-10 "><input placeholder="Kode pos" name="kode_pos" type="text" tabindex="1" required autofocus></div>
						</div>
						<div class="form-group row">
							<div class="col-md-2 col-form-label">Telepon : </div>
							<div class="col-md-10 "><input placeholder="Ttelepon" name="telepon" type="text" tabindex="1" required autofocus></div>
						</div>
						<div class="form-group row">
							<div class="col-md-2 col-form-label">Harga : </div>
							<div class="col-md-10 "><input placeholder="Harga" name="harga" type="number" min="0" tabindex="1" required autofocus></div>
						</div>
						<div class="form-group row">
							<div class="col-md-2 col-form-label">Nego :</div>
							<div class="col-md-10 ">
							<select name="nego" tabindex="1" required autofocus>
								<option value="tidak">Tidak</option>
								<option value="iya">Iya</option>
							</select>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-2 col-form-label">Tipe : </div>
							<div class="col-md-10"><input placeholder="Tipe" name="tipe" type="text" tabindex="1" required autofocus></div>
						</div>
						<div class="form-group row">
							<div class="col-md-2 col-form-label">Deskripsi : </div>
							<div class="col-md-10"><textarea placeholder="Definisi" name="deskripsi" tabindex="1" required autofocus></textarea></div>
						</div>
						<div class="form-group row">
							<div class="col-md-2 col-form-label">Lokasi : </div>
							<div class="col-md-10"><input placeholder="Lokasi" name="koordinat" type="text" tabindex="1" required autofocus>&nbsp<a target="_blank" href=https://www.google.co.id/maps/@-6.9031504,107.5919395,15z?hl=en >Pilih lokasi</a></div>
						</div>

						<div class="form-group row">
							<div class="col-md-2"></div>
							<div class="col-md-10"><button name="submit" value="tambah" type="submit" id="contact-submit" data-submit="...Sending">Submit</button></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script src="<?php echo site_url() . 'js/libs/jquery.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/tether.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/bootstrap.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/script.js'; ?>"></script>
</body>

</html>
