	<nav class="navbar navbar-toggleable-md navbar-light">
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<a class="navbar-brand" href="<?php echo site_url('Admin/home'); ?>">
			<img src="" width="30" height="30" class="d-inline-block align-top" alt=""> Property
		</a>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">
				<?php if(!isset($_SESSION['logged_in'])):; ?>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('Page/register'); ?>">Daftar</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('Page/login'); ?>">Login</a>
					</li>
				<?php else: ?>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('Admin/home'); ?>">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('Admin/pageProperty'); ?>">Property</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('Admin/pagePaying'); ?>">Paying</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('Admin/logout'); ?>">Logout</a>
					</li>
				<?php endif; ?>
			</ul>
		</div>
	</nav>
