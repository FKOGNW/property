<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
	<title>Property - Temukan hunian idaman!</title>
	<link rel="stylesheet" href="<?php echo site_url() . 'css/bootstrap.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/font-awesome.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/app.css'; ?>" />
</head>

<body>
	<?php include('partials/navbar_admin.php'); ?>

	<div class="container mt-5">
		<div class="row">
			<div class="col-sm-12">
				<h3>Log Transaksi</h3>
				<table class="table">
					<tr>
						<th>No</th>
						<th>Id Transaksi</th>
						<th>Id User</th>
						<th>Id Property</th>
						<th>waktu Bayar</th>
						<th>waktu Beli</th>
						<th>Status Bayar</th>
					</tr>

					<?php $No= 1; foreach ($dataTransaksi as $transaki) { ?>
					<tr>
						<td><?php echo $No++; ?></td>
						<td><?php echo $transaki->id; ?></td>
						<td><?php echo $transaki->id_user; ?></td>
						<td><?php echo $transaki->id_property; ?></td>
						<td><?php echo $transaki->waktu_bayar; ?></td>
						<td><?php echo $transaki->buying_at; ?></td>
						<td><?php echo $transaki->status_bayar; ?></td>
					</tr>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>

	<script src="<?php echo site_url() . 'js/libs/jquery.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/tether.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/bootstrap.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/script.js'; ?>"></script>
</body>

</html>

