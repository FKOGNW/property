<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
	<title>Property - Temukan hunian idaman!</title>
	<link rel="stylesheet" href="<?php echo site_url() . 'css/bootstrap.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/font-awesome.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/app.css'; ?>" />
</head>

<body>
	<?php include('partials/navbar.php'); ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="container">
				<form id="contact" action="<?php echo site_url('Page/processRegister'); ?>" method="post">
					<h3>Form Registrasi User</h3>
					<div class="form-group row">
						<div class="col-md-2">Nama : </div>
						<div class="col-md-10"><input placeholder="Nama" name="nama" type="text" tabindex="1" required autofocus></div>
					</div>
					<div class="form-group row">
						<div class="col-md-2">Username : </div>
						<div class="col-md-10"><input placeholder="Username" name="username" type="text" tabindex="1" required autofocus></div>
					</div>
					<div class="form-group row">
						<div class="col-md-2">Password : </div>
						<div class="col-md-10"><input placeholder="Password" name="password" type="password" tabindex="1" required autofocus></div>
					</div>

					<div class="form-group row">
						<button name="submit" value="tambah" type="submit" id="contact-submit" data-submit="...Sending">Submit</button>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>

	<script src="<?php echo site_url() . 'js/libs/jquery.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/tether.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/bootstrap.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/script.js'; ?>"></script>
</body>

</html>
