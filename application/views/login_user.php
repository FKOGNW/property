<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
	<title>Login</title>
	<link rel="stylesheet" href="<?php echo site_url() . 'css/bootstrap.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/font-awesome.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/app.css'; ?>" />
</head>

<body>
	<h1>Login</h1>

	<div class="container mt-5">
		<div class="row">
			<div class="col-sm-4">
				<form action="<?php echo site_url('Page/processLogin'); ?>" method="post">
					<div class="form-group">
						<input class="form-control" type="text" name="username" placeholder="username"><span class="highlight"></span><span class="bar"></span>
					</div>
					<div class="form-group">
						<input class="form-control" type="password" name="password" placeholder="password"><span class="highlight"></span><span class="bar">
					</div>
					<input class="btn btn-primary" type="submit" class="button buttonBlue">
				</form>
			</div>
		</div>
	</div>
	<script src="<?php echo site_url() . 'js/libs/jquery.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/tether.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/bootstrap.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/script.js'; ?>"></script>
</body>

</html>
