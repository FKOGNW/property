<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
	<title>Property - Temukan hunian idaman!</title>
	<link rel="stylesheet" href="<?php echo site_url() . 'css/bootstrap.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/font-awesome.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/app.css'; ?>" />
</head>

<body>
	<?php include('partials/navbar_admin.php'); ?>


	<div class="container-fluid mt-5">
		<div class="row">
			<div class="col-sm-12">
				<h1>List Property
					<small style="font-size:1.1rem">
						<a href="<?php echo site_url('Admin/tambahProperty'); ?>">Tambah Property</a>
					</small>
				</h1>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-sm-12">
				<table class="table">
					<tr>
						<td>No</td>
						<td>Nama pemilik</td>
						<td>Jalan</td>
						<td>Keluarahan</td>
						<td>Kecamatan</td>
						<td>Kota</td>
						<td>Kode pos</td>
						<td>Telepon</td>
						<td>Harga</td>
						<td>Nego</td>
						<td>Deskripsi</td>
						<td>Status</td>
						<td>Tipe</td>
						<td>Aksi</td>
					</tr>
					<?php $no= 1;?>
					<?php foreach ($dataProperty as $property) { ?>
						<tr>
							<td><?php echo $no++; ?></td>
							<td><?php echo $property->pemilik; ?></td>
							<td><?php echo $property->jalan; ?></td>
							<td><?php echo $property->kelurahan; ?></td>
							<td><?php echo $property->kecamatan; ?></td>
							<td><?php echo $property->kota; ?></td>
							<td><?php echo $property->kode_pos; ?></td>
							<td><?php echo $property->telepon; ?></td>
							<td><?php echo $property->harga; ?></td>
							<td><?php echo $property->nego; ?></td>
							<td><?php echo $property->deskripsi; ?></td>
							<td><?php echo $property->status; ?></td>
							<td><?php echo $property->tipe; ?></td>
							<td>
								<a href="<?php echo site_url('Admin/updateProperty/'.$property->id); ?>">Edit</a> ||
								<a href="<?php echo site_url('Admin/deleteProperty/'.$property->id); ?>">Delete</a>
							</td>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>

	<script src="<?php echo site_url() . 'js/libs/jquery.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/tether.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/bootstrap.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/script.js'; ?>"></script>
</body>

</html>
