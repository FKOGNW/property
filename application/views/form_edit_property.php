<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
	<title>Property - Temukan hunian idaman!</title>
	<link rel="stylesheet" href="<?php echo site_url() . 'css/bootstrap.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/font-awesome.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/app.css'; ?>" />
</head>

<body>
	<?php include('partials/navbar_admin.php'); ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<form id="contact" action="<?php echo site_url('Admin/prosesUpdateProperty'); ?>" method="post">
					<h3>Form Update Property</h3>
					<div class="form-group row">
						<div class="col-md-2 col-form-label">Nama pemilik : </div>
						<div class="col-md-10"><input placeholder="Nama pemilik" name="pemilik" type="text" value="<?php echo $property->pemilik?>" tabindex="1" required autofocus></div>
					</div>
					<div class="form-group row">
						<div class="col-md-2 col-form-label">Jalan : </div>
						<div class="col-md-10"><input placeholder="Jalan" name="jalan" type="text" value="<?php echo $property->jalan?>" tabindex="1" required autofocus></div>
					</div>
					<div class="form-group row">
						<div class="col-md-2 col-form-label">Kelurahan : </div>
						<div class="col-md-10"><input placeholder="Kelurahan" name="kelurahan" type="text" value="<?php echo $property->kelurahan?>" tabindex="1" required autofocus></div>
					</div>
					<div class="form-group row">
						<div class="col-md-2 col-form-label">Kecamatan : </div>
						<div class="col-md-10"><input placeholder="Kecamatan" name="kecamatan" type="text" value="<?php echo $property->kecamatan?>" tabindex="1" required autofocus></div>
					</div>
					<div class="form-group row">
						<div class="col-md-2 col-form-label">Kota : </div>
						<div class="col-md-10"><input placeholder="Kota" name="kota" type="text" value="<?php echo $property->kota?>" tabindex="1" required autofocus></div>
					</div>
					<div class="form-group row">
						<div class="col-md-2 col-form-label">Kode pos : </div>
						<div class="col-md-10"><input placeholder="Kode pos" name="kode_pos" type="text" value="<?php echo $property->kode_pos?>" tabindex="1" required autofocus></div>
					</div>
					<div class="form-group row">
						<div class="col-md-2 col-form-label">Telepon : </div>
						<div class="col-md-10"><input placeholder="Ttelepon" name="telepon" type="text" value="<?php echo $property->telepon?>" tabindex="1" required autofocus></div>
					</div>
					<div class="form-group row">
						<div class="col-md-2 col-form-label">Harga : </div>
						<div class="col-md-10"><input placeholder="Harga" name="harga" type="number" min="0" value="<?php echo $property->harga?>" tabindex="1" required autofocus></div>
					</div>
					<div class="form-group row">
						<div class="col-md-2 col-form-label">Nego :</div>
						<select name="nego">
							<option value="tidak">Tidak</option>
							<option value="iya">Iya</option>
						</select>
					</div>
					<div class="form-group row">
						<div class="col-md-2 col-form-label">Tipe : </div>
						<div class="col-md-10"><input placeholder="Tipe" name="tipe" type="text" tabindex="1" value="<?php echo $property->tipe?>" required autofocus></div>
					</div>
					<div class="form-group row">
						<div class="col-md-2 col-form-label">Deskripsi : </div><textarea placeholder="Definisi" name="deskripsi" value="<?php echo $property->deskripsi?>"></textarea>
					</div>
					<input type="hidden" name="id" value="<?php echo $property->id?>">
					<div class="form-group row">
						<button name="submit" value="tambah" type="submit" id="contact-submit" data-submit="...Sending">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<script src="<?php echo site_url() . 'js/libs/jquery.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/tether.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/bootstrap.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/script.js'; ?>"></script>
</body>

</html>
