<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
	<title>Property - Temukan hunian idaman!</title>
	<link rel="stylesheet" href="<?php echo site_url() . 'css/bootstrap.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/font-awesome.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/app.css'; ?>" />
</head>

<body>
<?php include('partials/navbar.php'); ?>

	<div class="jumbotron jumbotron-fluid" style="background-color: white;">
		<div class="container">
			<h1 class="display-4" style="width: 40rem;">Property membantu anda menemukan hunian idaman!</h1>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>Kode Pembayaran</h1>
				<h2><?php echo $kode_pembayaran ?></h2>
			</div>
		</div>
	</div>

	<script src="<?php echo site_url() . 'js/libs/jquery.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/tether.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/bootstrap.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/script.js'; ?>"></script>
</body>

</html>

