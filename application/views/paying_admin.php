<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
	<title>Property - Temukan hunian idaman!</title>
	<link rel="stylesheet" href="<?php echo site_url() . 'css/bootstrap.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/font-awesome.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/app.css'; ?>" />
</head>

<body>
	<?php include('partials/navbar_admin.php'); ?>

	<div class="container mt-5">
		<div class="row">
			<div class="col-sm-4">
				<form id="contact" action="<?php echo site_url('Admin/prosesPembayaran'); ?>" method="post">
					<h3>Bayar Property</h3>
					<div class="form-group row mt-4">
						<label class="col-sm-5 col-form-label">Kode Pembayaran</label>
						<div class="col-sm-7">
							<input class="form-control" placeholder="Kode Pembayaran" name="kode_pembayaran" type="text" tabindex="1" required autofocus>
						</div>
					</div>
					<button class="btn btn-primary" name="submit" value="tambah" type="submit" id="contact-submit" data-submit="...Sending">Submit</button>
				</form>
			</div>
		</div>
	</div>
	<script src="<?php echo site_url() . 'js/libs/jquery.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/tether.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/bootstrap.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/script.js'; ?>"></script>
</body>

</html>

