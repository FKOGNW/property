<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
	<title>Property - Temukan hunian idaman!</title>
	<link rel="stylesheet" href="<?php echo site_url() . 'css/bootstrap.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/font-awesome.min.css'; ?>" />
	<link rel="stylesheet" href="<?php echo site_url() . 'css/app.css'; ?>" />
</head>

<body>
<?php include('partials/navbar.php'); ?>

	<div class="jumbotron jumbotron-fluid" style="background-color: white;">
		<div class="container">
			<h1 class="display-4" style="width: 40rem;">Detail Rumah milik <?php echo $property->pemilik?>.</h1>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<p>Nama pemilik : <?php echo $property->pemilik?></p>
				<p>Jalan : <?php echo $property->jalan?></p>
				<p>Kelurahan : <?php echo $property->kelurahan?></p>
				<p>Kecamatan : <?php echo $property->kecamatan?></p>
				<p>Kota : <?php echo $property->kota?></p>
				<p>Kode pos : <?php echo $property->kode_pos?></p>
				<p>Telepon : <?php echo $property->telepon?></p>
				<p>Harga : <?php echo $property->harga?></p>
				<p>Nego : <?php echo $property->nego?></p>
				<p>Status : <?php echo $property->status?></p>
				<p>Tipe : <?php echo $property->tipe?></p>
			</div>
			<div class="col-md-6">
				<?php echo $property->koordinat?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
			    	<?php if($property->status!=("Sudah terjual") && isset($_SESSION['logged_in'])){?>
					    <form action="<?php echo site_url('Page/buyingProcess'); ?>" method="post">
				    	<input type="hidden" name="id_user" value="<?php echo $this->session->id_user?>">
				    	<input type="hidden" name="id_property" value="<?php echo $property->id?>">
			    		<button name="submit" type="submit" value="Beli">Beli</button>
			    	<?php }else{?>
			    		<button name="submit" type="submit" value="Beli">Beli</button>
			    	<?php }?>	
			    </form>
			</div>
		</div>
	</div>

	<script src="<?php echo site_url() . 'js/libs/jquery.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/tether.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/libs/bootstrap.min.js'; ?>"></script>
	<script src="<?php echo site_url() . 'js/script.js'; ?>"></script>
</body>

</html>

